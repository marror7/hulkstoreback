/**Creates**/

/**Create brand**/
CREATE TABLE IF NOT EXISTS  brand (
	id serial NOT NULL,
	created_date timestamp NULL,
	name varchar(255) NULL,
	CONSTRAINT brand_pkey PRIMARY KEY (id)
);

/**Create product type**/
CREATE TABLE IF NOT EXISTS product_type (
	id serial NOT NULL,
	created_date timestamp NULL,
	"name" varchar(255) NULL,
	CONSTRAINT product_type_pkey PRIMARY KEY (id)
);

/**Create product**/
CREATE TABLE IF NOT EXISTS product (
	id serial NOT NULL,
	code varchar(255) NULL,
	created_date timestamp NULL,
	description varchar(500) NULL,
	"name" varchar(255) NULL,
	price int4 NULL,
	quantity int4 NULL,
	brand_id int4 NULL,
	product_type_id int4 NULL,
	CONSTRAINT product_pkey PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS product ADD CONSTRAINT fklabq3c2e90ybbxk58rc48byqo FOREIGN KEY (product_type_id) REFERENCES product_type(id);
ALTER TABLE IF EXISTS product ADD CONSTRAINT fks6cydsualtsrprvlf2bb3lcam FOREIGN KEY (brand_id) REFERENCES brand(id);


/**Create user role**/
CREATE TABLE IF NOT EXISTS role(
	id serial NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT role_pkey PRIMARY KEY (id)
);

/**Create users**/
CREATE TABLE IF NOT EXISTS users (
	id serial NOT NULL,
	created_date timestamp NULL,
	complete_name varchar(255) NULL,
	role_id int4 NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS users ADD CONSTRAINT fk4qu1gr772nnf6ve5af002rwya FOREIGN KEY (role_id) REFERENCES role(id);

/**Create transaccion type**/
CREATE TABLE IF NOT EXISTS transaction_type (
	id serial NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT transaction_type_pkey PRIMARY KEY (id)
);

/**Create record history**/
CREATE TABLE IF NOT EXISTS record_history (
	id serial NOT NULL,
	created_date timestamp NULL,
	quantity int4 NULL,
	product_id int4 NULL,
	seller_id int4 NULL,
	transaction_type_id int4 NULL,
	CONSTRAINT record_history_pkey PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS record_history ADD CONSTRAINT fkci39pq9kk4u1109b8970nr3ew FOREIGN KEY (transaction_type_id) REFERENCES transaction_type(id);
ALTER TABLE IF EXISTS record_history ADD CONSTRAINT fkhipb9curkeg4hdeygaod3rws3 FOREIGN KEY (product_id) REFERENCES product(id);
ALTER TABLE IF EXISTS record_history ADD CONSTRAINT fkt2i76xd8qchllbrrh9sf4perp FOREIGN KEY (seller_id) REFERENCES users(id);
