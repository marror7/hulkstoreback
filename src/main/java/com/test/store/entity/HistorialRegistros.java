package com.test.store.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "record_history")
public class HistorialRegistros implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "quantity")
	private Integer quantity;

	@OneToOne
	@JoinColumn(name = "product_id")
	private Producto productId;

	@OneToOne
	@JoinColumn(name = "seller_id")
	private Usuario sellerId;

	@OneToOne
	@JoinColumn(name = "transaction_type_id")
	private TipoTransaccion transactionTypeId;

	@Column
	@CreationTimestamp
	private Date createdDate;

	public HistorialRegistros(Integer id, Integer quantity, Producto productId, Usuario sellerId,
			TipoTransaccion transactionTypeId, Date createdDate) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.productId = productId;
		this.sellerId = sellerId;
		this.transactionTypeId = transactionTypeId;
		this.createdDate = createdDate;
	}

	public HistorialRegistros() {
		super();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Producto getProductId() {
		return productId;
	}

	public void setProductId(Producto productId) {
		this.productId = productId;
	}

	public Usuario getSellerId() {
		return sellerId;
	}

	public void setSellerId(Usuario sellerId) {
		this.sellerId = sellerId;
	}

	public TipoTransaccion getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(TipoTransaccion transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
