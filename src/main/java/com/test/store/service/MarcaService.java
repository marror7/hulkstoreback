/**
 * 
 */
package com.test.store.service;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.store.entity.Marca;
import com.test.store.repository.MarcaRepository;

/**
 * 
 *
 */

@Service
public class MarcaService {


	@Autowired
	MarcaRepository marcaRepository;

	/**
	 * method that creates a new instance of a Marca
	 * @param newBrand
	 * @return
	 */
	public Marca save(@Valid Marca newBrand) {
		return marcaRepository.save(newBrand);
	}

}
