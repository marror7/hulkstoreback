package com.test.store.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.test.store.entity.Producto;
import com.test.store.repository.ProductoRepository;

import request.AdminProductoRequest;

/**
 * 
 *
 */
@Service
public class ProductoService {

	@Autowired
	ProductoRepository productoRepository;

	/**
	 * Methot that creates a new instance of a producto
	 * 
	 * @param newProduct
	 * @return Producto
	 */
	public Producto save(@Valid Producto newProduct) {
		return productoRepository.save(newProduct);
	}

	/**
	 * Methot that return all the products.
	 * 
	 * @return List<Producto>
	 */
	public List<Producto> getAll() {
		return (List<Producto>) productoRepository.findAll();
	}

	/**
	 * Method that return a unique producto searched it by id
	 * 
	 * @param id
	 * @return Producto
	 */
	public Producto findOne(Integer id) {
		return productoRepository.findById(id).get();
	}

	/**
	 * Method that filter the products searching by name or code
	 * 
	 * @param filter
	 * @return List<Producto>
	 */
	public List<Producto> filter(String filter) {
		return productoRepository.filterProductByNameOrCode(filter);
	}

	/**
	 * 
	 * @param newProductRequest
	 * @return
	 */
	public Producto addStock(AdminProductoRequest newProductRequest) {
		Producto auxProduct = this.productoRepository.findById(newProductRequest.getProduct().getId()).get();
		auxProduct.setQuantity(auxProduct.getQuantity() + newProductRequest.getQuantity());
		return this.productoRepository.save(auxProduct);
	}

	/**
	 * 
	 * @param newProductRequest
	 * @return
	 */
	public Producto minusStock(AdminProductoRequest newProductRequest) {
		Producto auxProduct = this.productoRepository.findById(newProductRequest.getProduct().getId()).get();
		auxProduct.setQuantity(auxProduct.getQuantity() - newProductRequest.getQuantity());
		return this.productoRepository.save(auxProduct);
	}
}
