/**
 * 
 */
package com.test.store.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.constant.ApiConstant;
import com.test.store.entity.Marca;
import com.test.store.service.MarcaService;

/**
 * 
 *
 */
@Controller
@RequestMapping(value =ApiConstant.BRAND_CONTROLLER_API)
public class MarcaController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MarcaService marcaService;
	
	@Autowired
	public MarcaController(MarcaService marcaService) {
		this.marcaService = marcaService;
	}
	
	@PostMapping(value = ApiConstant.BRAND_CONTROLLER_API_SAVE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Marca> save(@Valid @RequestBody Marca newBrand) {
		try {
			Marca auxBrand = marcaService.save(newBrand);
			return new ResponseEntity<>(auxBrand, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
