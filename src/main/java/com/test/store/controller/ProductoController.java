package com.test.store.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.constant.ApiConstant;
import com.test.store.entity.Producto;
import com.test.store.service.ProductoService;

import request.AdminProductoRequest;

/**
 * 
 *
 */
@Controller
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(value = ApiConstant.PRODUCT_CONTROLLER_API)
public class ProductoController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ProductoService productoService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping(value = ApiConstant.PRODUCT_CONTROLLER_API_GET_ALL, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Producto>> getAll() {
		try {
			List<Producto> listProduct= productoService.getAll();
			return new ResponseEntity<>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param filter
	 * @return
	 */
	@GetMapping(value = ApiConstant.PRODUCT_CONTROLLER_API_FILTER, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Producto>> filter(@RequestParam String filter) {
		try {
			List<Producto> listProduct= productoService.filter(filter);
			return new ResponseEntity<>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param adminProductoRequest
	 * @return
	 */
	@PostMapping(value = ApiConstant.PRODUCT_CONTROLLER_ADD, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> addProducts(@RequestBody AdminProductoRequest adminProductoRequest) {
		try {
			productoService.addStock(adminProductoRequest);
			return new ResponseEntity<>( HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 
	 * @param adminProductoRequest
	 * @return
	 */
	@PostMapping(value = ApiConstant.PRODUCT_CONTROLLER_REMOVE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> removeProducts(@RequestBody AdminProductoRequest adminProductoRequest) {
		try {
			
			productoService.minusStock(adminProductoRequest);
			return new ResponseEntity<>( HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

}
