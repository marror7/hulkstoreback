package com.test.store.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.test.store.entity.Producto;


/**
 * 
 *
 */
public interface ProductoRepository extends CrudRepository<Producto, Integer> {
	
	
	@Query(value = "select * from producto p where upper(p.name) like upper(concat('%', :filter, '%')) or to_char(p.id, 'FM9999') = :filter", nativeQuery = true)
	public List<Producto> filterProductByNameOrCode(@Param("filter") String filter);

	

}
