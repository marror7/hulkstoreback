/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.HistorialRegistros;

/**
 * 
 *
 */
public interface RecordHistoryRepository extends CrudRepository<HistorialRegistros, Integer>{

}
