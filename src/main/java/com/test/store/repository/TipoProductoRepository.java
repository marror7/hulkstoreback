/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.TipoProducto;

/**
 * 
 *
 */
public interface TipoProductoRepository extends CrudRepository<TipoProducto, Integer>{

}
