/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.Usuario;

/**
 * 
 *
 */
public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
