/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.Marca;

/**
 * 
 *
 */
public interface MarcaRepository extends CrudRepository<Marca, Integer> {

}
