/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.TipoTransaccion;

/**
 * 
 *
 */
public interface TipoTransaccionRepository extends CrudRepository<TipoTransaccion, Integer> {

}
