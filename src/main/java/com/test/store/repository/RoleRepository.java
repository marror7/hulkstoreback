package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.Rol;

/**
 * 
 *
 */
public interface RoleRepository extends CrudRepository<Rol, Integer> {

}
