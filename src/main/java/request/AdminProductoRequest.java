/**
 * 
 */
package request;

import com.test.store.entity.Producto;

/**
 * 
 *
 */
public class AdminProductoRequest {

	Producto producto;
	Integer quantity;

	public AdminProductoRequest() {
		super();
	}

	public AdminProductoRequest(Producto producto, Integer quantity) {
		super();
		this.producto = producto;
		this.quantity = quantity;
	}

	public Producto getProduct() {
		return producto;
	}

	public void setProduct(Producto producto) {
		this.producto = producto;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
