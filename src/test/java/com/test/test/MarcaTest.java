/**
 * 
 */
package com.test.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import com.test.store.HulkStoreApplication;
import com.test.store.controller.MarcaController;
import com.test.store.entity.Marca;
import com.test.store.service.MarcaService;

/**
 * 
 *
 */
@ContextConfiguration(classes = HulkStoreApplication.class)
@WebMvcTest({ MarcaController.class })
@TestMethodOrder(Alphanumeric.class)
class MarcaTest {

	MarcaController marcaController;
	
	@MockBean
	MarcaService marcaService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@BeforeEach
	public void setup() {
		marcaService = Mockito.mock(MarcaService.class);
		marcaController = new MarcaController(marcaService);
	}

	@Test
	void createTestValid() {
		 Marca marca = new Marca(null, "prueba", null);
		 when(marcaService.save(marca)).thenReturn(marca);
		 ResponseEntity<Marca> httpResponse = marcaController.save(marca);
		 Assertions.assertEquals(HttpStatus.OK, httpResponse.getStatusCode());
		 Assertions.assertEquals("prueba", httpResponse.getBody().getName());
		 
		logger.info("start test");
		assertTrue(true);
		logger.info("finish test");

	}
	
	@Test
	void createTestExeption() {
		 Marca marca = new Marca(null, null, null);
		 when(marcaService.save(marca)).thenReturn(marca);
		 ResponseEntity<Marca> httpResponse = marcaController.save(marca);
		 Assertions.assertEquals(HttpStatus.BAD_REQUEST, httpResponse.getStatusCode());

	}

}
